package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;


public class PlayerTest {
//////////////////////////////////////////// increase
	@Test
	public void scoreShouldBeIncreased() {
		// Arrange
		Player player = new Player("rinomenoventipunti", 0);
		
		// Action
		player.incrementScore();
		
		// Assert
		assertEquals(1, player.getScore());
	}
	
	@Test
	public void scoreShouldBeNotIncreased() {
		Player player = new Player("rinomenoventipunti", 0);
		
		// Action
		//player.incrementScore();
		
		// Assert
		assertEquals(0, player.getScore());
	}
//////////////////////////////////////////// score as string
	@Test
	public void shouldReturnLove() {
		// Arrange
		Player player = new Player("rinomenoventipunti", 0);
		
		// Action
		String scoreAsString = player.getScoreAsString();
		
		// Assert
		assertEquals("love", scoreAsString);
		
	}
	@Test
	public void shouldReturnFifteen() {
		// Arrange
		Player player = new Player("rinomenoventipunti", 1);
		
		// Action
		String scoreAsString = player.getScoreAsString();
		
		// Assert
		assertEquals("fifteen", scoreAsString);
	}
	@Test
	public void shouldReturnThirty() {
		// Arrange
		Player player = new Player("rinomenoventipunti", 2);
		
		// Action
		String scoreAsString = player.getScoreAsString();
		
		// Assert
		assertEquals("thirty", scoreAsString);
	}
	@Test
	public void shouldReturnForty() {
		// Arrange
		Player player = new Player("rinomenoventipunti", 3);
		
		// Action
		String scoreAsString = player.getScoreAsString();
		
		// Assert
		assertEquals("forty", scoreAsString);
		
	}
	@Test
	public void shouldReturnNullNegative() {
		// Arrange
		Player player = new Player("rinomenoventipunti", -1);
		
		// Action
		String scoreAsString = player.getScoreAsString();
		
		// Assert
		assertNull(scoreAsString);
	}
	@Test
	public void shouldReturnNullPositive() {
		// Arrange
		Player player = new Player("rinomenoventipunti", 4);
		
		// Action
		String scoreAsString = player.getScoreAsString();
		
		// Assert
		assertNull(scoreAsString);
	}
	
//////////////////////////////////////////////// is tie with  
	@Test
	public void shouldBeTie() {
		// Arrange
		Player player1 = new Player("rinomenoventipunti", 1);
		Player player2 = new Player("mario", 1);
		
		// Action
		boolean tie = player1.isTieWith(player2);
		
		// Assertion
		assertTrue(tie);
	}
	
	@Test
	public void shouldBeNotTie() {
		// Arrange
		Player player1 = new Player("rinomenoventipunti", 1);
		Player player2 = new Player("mario", 2);
		
		// Action
		boolean tie = player1.isTieWith(player2);
		
		// Assertion
		assertFalse(tie);
	}

////////////////////////////////////////////////// hasAtLeastFortyPoints
	
	@Test
	public void shouldHaveAtLeastForty() {
		
		// Arrange
		Player player = new Player("mario", 3);
		
		// Action
		boolean forty = player.hasAtLeastFortyPoints();
		
		// Assert
		assertTrue(forty);
		
	}
	
	@Test
	public void shouldNotHaveAtLeastForty() {
		
		// Arrange
		Player player = new Player("mario", 2);
		
		// Action
		boolean forty = player.hasAtLeastFortyPoints();
		
		// Assert
		assertFalse(forty);
	}
	
///////////////////////////////////////////// hasLessThanFortyPoints
	@Test
	public void shouldHaveLessThanFortyPoints(){
		
		// Arrange
		Player player = new Player("mario", 2);
		
		// Action
		boolean forty = player.hasLessThanFortyPoints();
		
		// Assert
		assertTrue(forty);
		
	}
	
	@Test
	public void shouldNotHaveLessThanFortyPoints(){
		
		// Arrange
		Player player = new Player("mario", 4);
		
		// Action
		boolean forty = player.hasLessThanFortyPoints();
		
		// Assert
		assertFalse(forty);
		
	}
	
///////////////////////////////////////////// hasMoreThanFourtyPoints
	
	@Test
	public void shouldHaveMoreThanFortyPoints(){
		
		// Arrange
		Player player = new Player("mario", 4);
		
		// Action
		boolean forty = player.hasMoreThanFourtyPoints();
		
		// Assert
		assertTrue(forty);
		
	}
	
	@Test
	public void shouldNotHaveMoreThanFortyPoints(){
		
		// Arrange
		Player player = new Player("mario", 1);
		
		// Action
		boolean forty = player.hasMoreThanFourtyPoints();
		
		// Assert
		assertFalse(forty);
		
	}

/////////////////////////////////////////////// hasOnePointAdvantageOn
	
	@Test
	public void shouldHaveOnePointAdvantageOn() {
		// Arrange
		Player player1 = new Player("mario", 2);
		Player player2 = new Player("mario1", 1);
		
		// Action
		boolean adv = player1.hasOnePointAdvantageOn(player2);
		
		// Assert
		assertTrue(adv);
	}
	
	@Test
	public void shouldNotHaveOnePointAdvantageOn() {
		// Arrange
		Player player1 = new Player("mario", 1);
		Player player2 = new Player("mario1", 3);
		
		// Action
		boolean adv = player1.hasOnePointAdvantageOn(player2);
		
		// Assert
		assertFalse(adv);
	}
	
//////////////////////////////////////////// hasAtLeastTwoPointsAdvantageOn
	
	@Test
	public void shouldHaveAtLeastTwoPointsAdvantageOn() {
		// Arrange
		Player player1 = new Player("mario", 3);
		Player player2 = new Player("mario1", 1);
		
		// Action
		boolean adv = player1.hasAtLeastTwoPointsAdvantageOn(player2);
		
		// Assert
		assertTrue(adv);
	}
	
	@Test
	public void shouldNotHaveAtLeastTwoPointsAdvantageOn() {
		// Arrange
		Player player1 = new Player("mario", 3);
		Player player2 = new Player("mario1", 2);
		
		// Action
		boolean adv = player1.hasAtLeastTwoPointsAdvantageOn(player2);
		
		// Assert
		assertFalse(adv);
	}
}
